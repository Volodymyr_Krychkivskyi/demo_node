import { REMOVE_TODOS, TOGGLE_TODOS_STATUS, SET_TODOS } from "../action-types";

export const setTodos = (payload) => ({ type: SET_TODOS, payload });
export const toggleTodosStatus = () => ({ type: TOGGLE_TODOS_STATUS });
export const removeTodos = () => ({ type: REMOVE_TODOS });

export const fetchTodos = () => async (dispatch) => {
  try {
    const response = await fetch("https://jsonplaceholder.typicode.com/todos");
    const data = await response.json();
    dispatch(setTodos(data));
  } catch (e) {
    console.log(e);
  }
};
