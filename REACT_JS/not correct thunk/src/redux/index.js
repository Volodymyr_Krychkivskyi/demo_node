import { createStore, applyMiddleware } from "redux";
import { reduser } from "./redusers";
import thunk from "redux-thunk";

export const store = createStore(reduser, applyMiddleware(thunk));

export * from "./action-creators";
export * from "./action-types";
