import { combineReducers } from "redux";

import todosReduser from "./todos-reduser";

export const reduser = combineReducers({
  todos: todosReduser
});
