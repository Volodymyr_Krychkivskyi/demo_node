import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchTodos, incCounter, decCounter, resetCounter } from "./redux";
import "./styles.css";

export default function App() {
  const { todos } = useSelector(
    ({todos: { todos } }) => ({
      todos
    })
  );

  const dispatch = useDispatch();

  const handleInc = () => dispatch(incCounter());
  const handleDec = () => dispatch(decCounter());
  const handleReset = () => dispatch(resetCounter());

  useEffect(() => {
    fetchTodos();
  }, []);

  return (
    <div className="App">
      <button onClick={handleInc}>inc</button>
      <button onClick={handleDec}>Idec</button>
      <button onClick={handleReset}>reset</button>

      {todos.map((todo) => (
        <h2>
          {todo.id} - {todo.title}
        </h2>
      ))}

      <h2>Start editing to see some magic happen!</h2>
    </div>
  );
}
