import { combineReducers } from "redux";

import counterReduser from "./counter-reduser";
import todosReduser from "./todos-reduser";

export const reduser = combineReducers({
  counter: counterReduser,
  todos: todosReduser
});
