import { createStore } from "redux";
import { reduser } from "./redusers";

export const store = createStore(reduser);

export * from "./action-creators";
export * from "./action-types";
