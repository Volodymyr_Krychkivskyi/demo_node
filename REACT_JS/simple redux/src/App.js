import { useSelector, useDispatch } from "react-redux";
import { incCounter, decCounter, resetCounter } from "./redux";
import "./styles.css";

export default function App() {
  const { todos, counter } = useSelector(
    ({ counter: { counter }, todos: { todos } }) => ({
      counter,
      todos,
    })
  );

  const dispatch = useDispatch();

  const handleInc = () => dispatch(incCounter());
  const handleDec = () => dispatch(decCounter());
  const handleReset = () => dispatch(resetCounter());

  return (
    <div className="App">
      <h1>Counter: {counter}</h1>
      <button onClick={handleInc}>inc</button>
      <button onClick={handleDec}>Idec</button>
      <button onClick={handleReset}>reset</button>

      <h1>Todos: {todos}</h1>

      <h2>Start editing to see some magic happen!</h2>
    </div>
  );
}
