const fs = require('fs').promises;

  (async () => {
    try {
      const data = await fs.readFile('./test_main_dir/test_main_file2.js', 'utf8');
      console.log(data);
    } catch {
      console.error(err);
    }
  })();

  (async () => {
    try {
      fs.writeFile('./test_main_dir/test_main_file2.js', JSON.stringify({a:1, b:2}), 'utf8')
    } catch {
      console.error(err);
    }
  })();


  (async () => {
    try {
      fs.rename('./test_main_dir/test_main_file2.js', './test_main_file2.js')
    } catch {
      console.error(err);
    }
  })();

  (async () => {
    try {
      const readDirr = await fs.readdir('./test_main_dir')
    } catch {
      console.error(err);
    }
  })();





